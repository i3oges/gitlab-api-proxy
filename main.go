package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/api/", handler)
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	log.Printf("gitlab-api: listening on port: %s", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}

func errorOut(err error) {
	fmt.Printf("%s", err)
}

func handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "X-Requested-With")
	if r.Method == "GET" {
		client := &http.Client{}
		url := "https://" + os.Getenv("GITLAB_URL") + "/api/v4/" + r.URL.Path[5:]
		if r.URL.RawQuery != "" {
			url += "?" + r.URL.RawQuery
		}
		fmt.Println(url)
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			errorOut(err)
		}
		req.Header.Set("Private-Token", os.Getenv("GITLAB_PRIVATE_TOKEN"))

		resp, err := client.Do(req)
		if err != nil {
			errorOut(err)
		}
		defer resp.Body.Close()

		contents, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			errorOut(err)
		}
		fmt.Fprintf(w, "%s\n", contents)
	}
}
